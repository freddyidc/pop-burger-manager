<?php
$usuario = $_POST['usuario'];
$clave = $_POST['clave'];
 
if(empty($usuario) || empty($clave)){
header("Location: ../index.html");
exit();
}

if('admin' == $clave && 'admin' == $usuario){
	session_start();
	$_SESSION['usuario'] = $usuario;
	header("Location: ../principal.html");
}else{
	header("Location: ../index.html");
	exit();
}
 
?>